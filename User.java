package com.dg.demo;
/*
 * private: class
 * default: class, trong package
 * protected: class, trong package, (ngo�i package + trong class)
 * public: all
 */
public class User {
	int id;//default
	protected String name;

	public User(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + "]";
	}

}
